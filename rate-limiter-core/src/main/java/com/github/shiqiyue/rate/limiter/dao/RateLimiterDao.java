package com.github.shiqiyue.rate.limiter.dao;

import com.github.shiqiyue.rate.limiter.entity.RateLimiterItem;

import java.util.List;

/***
 * 流量控制信息数据DAO
 * 
 * @author wwy
 *
 */
public interface RateLimiterDao {
	
	/***
	 * 返回符合url的流量控制信息
	 * 
	 * @param url
	 * @return
	 */
	public List<RateLimiterItem> matchItems(String url);
}
