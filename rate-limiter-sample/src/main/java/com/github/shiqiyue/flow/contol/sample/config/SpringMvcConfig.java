package com.github.shiqiyue.flow.contol.sample.config;

import com.github.shiqiyue.rate.limiter.fliter.RateLimiterFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/***
 * @author wwy <br/>
 *         两种流量控制方式,选择一种就行 <br/>
 *         1.通过servlet的filter<br/>
 *         2.通过springmvc的Interceptor
 * 
 * 
 */
@Configuration
public class SpringMvcConfig extends WebMvcConfigurerAdapter {
	
	@Autowired
	private CustomFlowControlConfigurer flowControlConfigurer;
	
	/***
	 * 使用servlet的filter
	 * 
	 * @return
	 */
	@Bean
	public FilterRegistrationBean testFilterRegistration() {
		
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new RateLimiterFilter(flowControlConfigurer));
		registration.addUrlPatterns("/*");
		registration.setName("flowControlFilter");
		registration.setOrder(1);
		return registration;
	}
	
	/***
	 * 使用springmvc的interceptor
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// FlowControlHandlerInterceptor flowControlHandlerInterceptor = new
		// FlowControlHandlerInterceptor(
		// flowControlConfigurer);
		// registry.addInterceptor(flowControlHandlerInterceptor).addPathPatterns("/**");
		
		super.addInterceptors(registry);
	}
	
}
